FROM ruby:3.2.2-slim-bullseye

RUN apt-get update -qq && apt-get install -y build-essential \
                                             # proper pager (for development)
                                             less \
                                             git \
                                             chromium \
                                             lsb-release \
                                             gnupg2 \
                                             wget \
                                             # for typhoeus gem
                                             curl
# bullseye only offers postgres 13 and pg_dump complains about version differences
# have to run if after the first apt for wget, gpg and lsb_release
# https://computingforgeeks.com/how-to-install-postgresql-14-on-debian/
RUN echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
                                             # libyaml not bundled anymore since 3.2
RUN apt-get -qq update && apt-get install -y libyaml-dev \
                                             # postgres client libraries
                                             libpq-dev \
                                             # needed for pg_dump
                                             postgresql-client-14
RUN gem update --system

RUN mkdir -p /app
WORKDIR /app

COPY ./Gemfile /app/Gemfile
COPY ./Gemfile.lock /app/Gemfile.lock
ENV BUNDLE_PATH=/bundle
ENV BUNDLE_CACHE_PATH=/bundle
ENV BUNDLE_CACHE_ALL=/bundle
ENV BUNDLE_DEFAULT_INSTALL_USES_PATH=/bundle
RUN bundle install

COPY . /app

RUN SECRET_KEY_BASE_DUMMY=1 bundle exec rake assets:precompile

COPY ./entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

EXPOSE 3000
