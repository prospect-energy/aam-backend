# README

AAM 

## Development

Just start with docker compose.

```
docker compose up
```

### Containers

It runs the rails app in `app` container, a `postgres` container and a `grafana` container.

The app web interface can be accessed from host at http://localhost:3003

The DB can be accessed from host at port 533 like so: `postgres://postgres:postgres@localhost:5433/aam_backend_development`

You will need to identify via Basic Auth to access everything except the public certificate quicklinks.

You will need an `.env` file in the root directory which contains at least the `POSTGRES_PASSWORD=postgres` line

### Seeding

During `rake db:setup` or `rake db:seed` the app will seed (see `db/seeds.rb`) the DB 

You can clear all tables with `rake db:truncate_all` and then replay it with `rake db:seed`.

### Running commands

Run rails and rake commands within the app container. 
Either as `docker compose exec app bundle exec <command>`.
Or within a shell in container `docker compose exec app bash` and there `bundle exec <command>`

### Setting things up

```
# install updated gem dependencies
docker compose exec app bundle install

# setup database or migrate
docker compose exec app bundle exec rake db:prepare

# generate a new migration
docker compose exec app bundle exec rails generate migration NameInCamelCase

# migrate your migration
docker compose exec app bundle exec rake db:migrate

# restart webserver (after changing non-autoloaded things like initializers)
docker compose restart app

# truncate db (DANGER deletes all DATA)
docker compose exec app bundle exec rake db:truncate_all

# seeding db (already done in entrypoint script, but sometimes you might to do it manually)
docker compose exec app bundle exec rake db:seed

# setup dashboards and make them public
docker compose exec app bundle exec rake offsets:setup_dashboards
```

## Production

In development using docker compose nothing needs to be set. It should *just work™* :)

### Mandatory ENV vars

* `RAILS_ENV` set to `production`
* `PUBLIC_URL`  base URL of app (no trailing slash), Used to generate quicklinks in QR codes.
* `GRAFANA_URL` base URL of Grafana (no trailing slash). Used to link the dashboards.
* `ADMIN_EMAIL` the email for the `admin` user in the frontend
* `ADMIN_PASS` the password for the `admin` user in the frontend
* `POSTGRES_URL` where does the app find the database, like `postgres://user:password@host:5432/db_name`
* `GRAFANA_SERVICE_TOKEN` Create this token in grafana, as an Admin Service Account. See above (Chicken and Egg)

### Current deployment

The app is accessible at: https://aam.a2ei.org/

Containers are started by docker compose via systemd.

The `docker-compose.yml` is taken from Repo and manipulated by ansible. See https://gitlab.com/a2ei/prospect/infrastructure
