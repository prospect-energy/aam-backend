begin
  AAM_Backend::Application::VERSION = "v#{File.read 'VERSION'}"
rescue
  raise "VERSION file not found or unreadable."
end