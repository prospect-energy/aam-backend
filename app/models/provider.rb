# frozen_string_literal: true

# Provider is the different backend providers that can be used to unlock the MCU
# it is related to different MCU's that can be unlocked by this provider
# different providers might use the same unock method (such as OpenPaygo)
class Provider < ApplicationRecord
  def self.ransackable_attributes(_auth_object = nil)
    %w[auth_type created_at id id_value password provider_name unlock_type updated_at url
       username]
  end

  def display_name
    provider_name
  end

  def as_json(options = {})
    # secret key should not be sent through the APIs
    options[:except] ||= %w[username password unlock_type auth_type]
    super(options)
  end

  def tokens(mcu_id, token_count)
    case unlock_type
    when Enums::ProviderType::PayGoOps
      PayGoProviders::PayGoOps.new(self).tokens(mcu_id, token_count)
    when Enums::ProviderType::Bbox
      PayGoProviders::Bbox.new(self).tokens(mcu_id, token_count)
    when Enums::ProviderType::Paygee
      PayGoProviders::Paygee.new(self).tokens(mcu_id, token_count)
    else
      raise HttpError.new("Unsupported Unlock type #{unlock_type}", 400, 'UNSUPPORTED_UNLOCK_TYPE')
    end
  end
end
