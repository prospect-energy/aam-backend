# frozen_string_literal: true

# MCU is the microcontroller unit in the AAM that is used to unlock the offset
# it is related to different unlock code providers that can unlock a specific MCU
# it is also related to a backend that is used to communicate with the MCU
class Mcu < ApplicationRecord
  require 'csv'
  belongs_to :provider
  belongs_to :organization

  enum status: [ :orphaned, :allocated, :installed, :disabled ]

  def self.ransackable_attributes(_auth_object = nil)
    %w[backend_id created_at id id_value mcu_id mcu_name provider_id secret_key updated_at starting_token token_count organization_id status mcu_version]
  end

  def self.ransackable_associations(_auth_object = nil)
    ['provider']
  end

  def as_json(options = {})
    # secret key should not be sent through the APIs
    options[:except] ||= %w[secret_key]
    super(options)
  end

  CSV_COLUMNS = %w[serial_number mcu_id secret_key starting_code]
  CSV_HEADERS = ["Serial Number", "MCU ID", "Secret Key", "Starting Token"]
  def self.preview_csv(csv_file)
    csv_data = CSV.read(csv_file.path, headers: true)
    mapped = csv_data.map {
      |row|

      hash = row.to_hash
      hash["mcu_id"] = hash['serial_number'].scan(/\d+/)[0].to_i
      hash.slice(*CSV_COLUMNS)
    }
    return OpenStruct.new({
      rows: mapped,
      headers: CSV_HEADERS
    })
  end

  def self.import_csv(csv_file, provider, organization)
    CSV.foreach(csv_file.path, headers: true) do |row |
      hash = row.to_hash

      serial_number = row['serial_number']
      mcu_id = serial_number.scan(/\d+/)[0].to_i
      secret_key = hash["secret_key"]
      starting_token = hash["starting_code"]

      mcu = Mcu.find_or_create_by(mcu_id: mcu_id)
      if mcu.new_record?
        mcu.mcu_name = "A2EI-AAM-%04d" % mcu_id
        mcu.token_count = 0
        mcu.updated_on = Date.new
        if mcu_id < 1000
          mcu.mcu_version = "v2"
        end
      end

      mcu.secret_key = secret_key
      mcu.starting_token = starting_token
      mcu.organization = organization
      mcu.provider = provider


      mcu.save!
    end
  end
end
