class McuSerialNumberInput < Formtastic::Inputs::StringInput

  def to_html
    input_wrapping do
        serial_number_tag_id = object.id

        builder.label(input_name, label_text, label_html_options) +
        builder.text_field(method, input_html_options.merge(onchange: "update_serial(this.value)", onkeyup: "update_serial(this.value)")) + 
        template.content_tag(:br) +
        builder.label(input_name, 'Serial Number', {bold: false}) +
        template.content_tag(:p, 'AAM%08d' % object.send(method).to_i, {id: serial_number_tag_id }) +
        template.javascript_tag(
            "
            const objectId = #{serial_number_tag_id}
            const object = document.getElementById(objectId)
            window.update_serial = function (target) {
                console.log(target)
                object.innerHTML = 'AAM' + String(target).padStart(8, '0')
            }
            "
        )
    end
  end

end