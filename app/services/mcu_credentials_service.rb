# frozen_string_literal: true

require 'digest'

class McuCredentialsService
  def self.authenticate_machine(machine_id:, password:)
    machine = Mcu.find_by(mcu_id: machine_id)
    raise HttpError.new('Machine not found', 404, 'MACHINE_NOT_FOUND') unless machine

    str_to_hash = "#{machine.secret_key}::#{machine.mcu_id}::#{DateTime.now.strftime('%Y-%m-%d')}"

    hash = Digest::SHA2.hexdigest str_to_hash

    # raise "Invalid Credentials" if hash != password
    raise HttpError.new('Invalid Credentials', 401, 'INVALID_CREDS') if hash != password

    # Return machine if authorized
    machine
  end
end
