# frozen_string_literal: true

require 'jwt'

class AuthTokensService
  def self.authenticate(token)
    raise HttpError.new('Unauthorized', 401, 'UNAUTHORIZED') unless token

    access_token_secret = Constants::ACCESS_TOKEN_SECRET

    begin
      data, = JWT.decode token, access_token_secret, true, { algorithm: 'HS256' }
    rescue JWT::ExpiredSignature
      raise HttpError.new('access token needs refresh', 440, 'ACCESS_TOKEN_NEEDS_REFRESH')
    rescue StandardError
      raise HttpError.new('Invalid access token', 403, 'INVALID_ACCESS_TOKEN')
    end

    raise HttpError.new('Unauthorized', 401, 'UNAUTHORIZED') if data['mcu_id'].nil?

    mcu = Mcu.find_by(mcu_id: data['mcu_id'])
    raise HttpError.new('Unauthorized', 401, 'UNAUTHORIZED') unless mcu

    mcu
  end

  def self.refresh(refresh_token)
    refresh_token_secret = Constants::REFRESH_TOKEN_SECRET

    begin
      data, = JWT.decode refresh_token, refresh_token_secret, true, { algorithm: 'HS256' }
    rescue JWT::ExpiredSignature
      raise HttpError.new('Refresh token gone', 410, 'REFRESH_TOKEN_GONE')
    rescue StandardError
      raise HttpError.new('Invalid refresh token', 403, 'INVALID_REFRESH_TOKEN')
    end
    if data['mcu_id'].nil?
      raise HttpError.new('Invalid token Data', 400,
                          'INVALID_TOKEN_DATA')
    end

    tokens = generate_tokens({ mcu_id: data['mcu_id'] })
    { access_token: tokens[:access_token] }
  end

  def self.generate_tokens(payload)
    access_token_secret = Constants::ACCESS_TOKEN_SECRET
    refresh_token_secret = Constants::REFRESH_TOKEN_SECRET

    payload_access = payload.merge({ exp: 30.hours.from_now.to_i })
    payload_refresh = payload.merge({ exp: 365.days.from_now.to_i })
    access_token = JWT.encode payload_access, access_token_secret
    refresh_token = JWT.encode payload_refresh, refresh_token_secret
    { access_token:, refresh_token: }
  end

  def self.validate(token, is_validate_refresh)
    access_token_secret = Constants::ACCESS_TOKEN_SECRET
    refresh_token_secret = Constants::REFRESH_TOKEN_SECRET

    secret = is_validate_refresh ? refresh_token_secret : access_token_secret

    begin
      data, = JWT.decode token, secret, true, { algorithm: 'HS256' }
    rescue JWT::ExpiredSignature
      raise HttpError.new('token expired', 410, 'TOKEN_GONE')
    rescue StandardError
      raise HttpError.new('Invalid token', 403, 'INVALID_TOKEN')
    end

    return { ok: true }
  end
end
