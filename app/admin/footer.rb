module ActiveAdmin
  module Views
    class Footer < Component

      def build(namespace)
        super :id => "footer"
        super :style => "text-align: left;"

        div do
          h3 "AAM Backend #{Date.today.year} (#{AAM_Backend::Application::VERSION})"
        end
      end

    end
  end
end