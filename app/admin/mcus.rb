# frozen_string_literal: true

ActiveAdmin.register Mcu do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  # :backend_id,
  permit_params :mcu_name, :mcu_id, :provider_id, :secret_key, :organization_id, :status, :starting_token
  #
  # or
  #
  # permit_params do
  #   permitted = [:mcu_name, :mcu_id, :backend_id, :provider_id, :secret_key]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  
  index do
    selectable_column
    column "Postgres Id", sortable: :id do |mcu| 
      link_to mcu.id, admin_mcu_path(mcu)
    end

    column :created_at
    column :updated_at
    column :mcu_name
    column :mcu_id
    column(:serial_number) { |mcu| 'AAM%08d' % mcu.mcu_id }
    column(:provider) { |mcu| link_to mcu.provider.provider_name, admin_provider_path(mcu.provider) }
    # column :secret_key
    column :token_count
    column :starting_token
    column :status
    column :updated_on
    column :mcu_version
    column(:organization) { |mcu| link_to mcu.organization.name, admin_organization_path(mcu.organization) if mcu.organization != nil }
    actions
    #  do |mcu|
      # item "Preview", alert: "AMAZING"
    # end
  end


  show do |mcu|
    attributes_table do
      row :mcu_name
      row :mcu_id
      row(:serial_number) { 'AAM%08d' % mcu.mcu_id }
      row(:provider) { mcu.provider.provider_name }
      row :starting_token
      row :token_count
      row :secret_key
      row :organization
      # row :mcu_version
    end
    active_admin_comments_for(resource)
  end

  form do |f|
    f.inputs do
      f.input :mcu_name
      f.input :mcu_id, as: :mcu_serial_number, label: 'Mcu Id'
      # f.input :mcu_id, as: :mcu_serial_number, label: 'Serial Number'
      # f.input :mcu_id, label: 'Mcu Id'
      f.input :provider_id,
              as: :select,
              include_blank: false,
              collection: Provider.all.collect { |provider|
                            [provider.provider_name, provider.id]
                          }
      f.input :secret_key
      f.input :organization
      f.input :starting_token

      f.input :status
    end
    f.actions
  end

  action_item :csv_upload, only: :index do
    link_to 'Upload CSV', action: 'upload_csv'
  end
  #
  collection_action :upload_csv do
    render 'admin/mcus/upload_csv'
  end


  collection_action :preview_csv, method: :post do
    if params[:file].present?
      data = Mcu.preview_csv(params[:file])
      @preview_headers = data.headers
      @preview_data = data.rows
      @selected_provider = Provider.find(params[:provider_id]) if params[:provider_id].present?
      @selected_organization = Organization.find(params[:organization_id]) if params[:organization_id].present?
      @file_param = params[:file]

    else
      flash[:error] = "Please upload a CSV file."
    end
    render 'admin/mcus/upload_csv'
  end

  collection_action :import_csv, method: :post do
    if params[:file].present? and params[:provider_id].present? and params[:organization_id].present?
      csv_file = params[:file]
      provider = Provider.find(params[:provider_id])
      organization = Organization.find(params[:organization_id])

      begin
        # Process the CSV file
        Mcu.import_csv(csv_file, provider, organization)
        redirect_to admin_mcus_path, notice: "CSV file imported successfully!"
      rescue => e
        flash[:error] = "An Error occurred while processing csv:" + e.to_s
        render 'admin/mcus/upload_csv'
      end
    else
      flash[:error] = "Please upload a CSV file and choose a provider and an organization."
      render 'admin/mcus/upload_csv'
    end
  end


end