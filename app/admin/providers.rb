# frozen_string_literal: true

ActiveAdmin.register Provider do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :provider_name, :unlock_type, :url, :username, :password, :auth_type
  #
  # or
  #
  # permit_params do
  #   permitted = [:provider_name, :unlock_type, :url, :username, :password, :auth_type]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  provider_types = Enums::ProviderType.constants.collect { |data| data }
  auth_types = Enums::ProviderAuthType.constants.collect { |data| data }

  form do |f|
    f.inputs do
      f.input :provider_name
      f.input :unlock_type, as: :select, collection: provider_types, null: false, include_blank: false
      f.input :url
      f.input :username
      f.input :password, as: :text
      f.input :auth_type, as: :select, collection: auth_types, null: false, include_blank: false
    end
    f.actions
  end
end
