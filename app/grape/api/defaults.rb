# frozen_string_literal: true

module Api
  module Defaults
    extend ActiveSupport::Concern

    included do
      prefix 'api'
      version 'v1', using: :path
      default_format :json
      format :json

      helpers do
        include ActionController::HttpAuthentication::Token

        def authenticate
          @machine = machine_by_token token
        end

        def token
          headers['authorization'].blank? ? nil : token_params_from(headers['authorization']).shift[1]
        end

        def machine_by_token(token)
          AuthTokensService.authenticate(token)
        end
      end
    end
  end
end
