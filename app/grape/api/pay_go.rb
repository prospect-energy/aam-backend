# frozen_string_literal: true

module Api
  class PayGo < Grape::API
    include Api::Defaults
    use Middlewares::HttpErrorMiddleware
    resource :pay_go do
      params do
        requires :count, type: Integer, desc: 'Token count'
      end
      get 'tokens/:count' do
        authenticate
        @machine.provider.tokens(@machine.mcu_id, params[:count].to_i)
      end

      params do
        requires :mcu_id, type: String, desc: 'MCU ID'
        requires :count, type: Integer, desc: 'Token Count.'
      end
      get 'tokens/:mcu_id/:count' do
        raise HttpError.new('Unauthorized', 401, 'UNAUTHORIZED') unless Constants::ALLOW_PUBLIC_TOKENS

        @machine = Mcu.find_by(mcu_id: params[:mcu_id])
        raise HttpError.new('MCU not found', 400, 'MCU_NOT') unless @machine.present?
        @machine.provider.tokens(@machine.mcu_id, params[:count].to_i)
      end
    end

    resource :mcu do
      get :me do
        authenticate
        @machine
      end
    end
  end
end
