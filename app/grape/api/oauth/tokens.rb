# frozen_string_literal: true

module Api
  module Oauth
    class Tokens < Grape::API
      include Api::Defaults
      use Middlewares::HttpErrorMiddleware

      resource :oauth do
        params do
          requires :username, type: String
          requires :password, type: String
        end
        post :auth do
          body = declared(params)
          machine = McuCredentialsService.authenticate_machine(machine_id: body[:username],
                                                               password: body[:password])
          AuthTokensService.generate_tokens({ mcu_id: machine.mcu_id })
        end

        params do
          requires :refresh_token, type: String
        end
        post :refresh do
          body = declared(params)
          AuthTokensService.refresh(body[:refresh_token])
        end

        resource :validate do
          params do
            requires :token, type: String
          end
          post :refresh_token do
            body = declared(params)
            AuthTokensService.validate(body[:token], true)
          end

          params do
            requires :token, type: String
          end
          post :access_token do
            body = declared(params)
            AuthTokensService.validate(body[:token], false)
          end
        end

      end
    end
  end
end
