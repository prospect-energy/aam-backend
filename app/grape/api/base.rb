# frozen_string_literal: true

module Api
  class Base < Grape::API
    mount Api::PayGo
    mount Api::Oauth::Tokens
  end
end
