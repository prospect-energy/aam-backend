# frozen_string_literal: true

module Middlewares
  class HttpErrorMiddleware < Grape::Middleware::Error
    def call!(env)
      @env = env
      before

      begin
        @app_response = @app.call(@env)
        after || @app_response
      rescue HttpError => e
        puts 'Error Caught by HttpErrorMiddleware'
        error!(e.body, e.status)
      rescue StandardError => e
        error!(e.message, 500)
      end
    end
  end
end
