# frozen_string_literal: true

class HttpError < StandardError
  attr_reader :message, :status, :error_key

  def initialize(message, status, error_key)
    @message = message
    @status = status
    @error_key = error_key
    super(message)
  end

  def body
    { message: @message, status: @status, errorKey: error_key }
  end
end
