# frozen_string_literal: true

module PayGoProviders
  class Bbox
    include HTTParty
    # base_uri 'https://credit-service.bboxxdev.co.uk'
    base_uri 'https://credit-service.bboxx.co.uk'

    def initialize(provider, options = {})
      @options = options
      @provider = provider
      @auth_token = ''
    end

    def headers
      { 'Content-Type' => 'application/json' }
    end

    def headers_with_auth
      { 'Content-Type' => 'application/json; charset=UTF-8',
        'Authorization' => "Bearer #{@auth_token}" }
    end

    def authenticate_bbox
      data = { 'userName' => @provider.username, 'password' => @provider.password }
      res = self.class.post('https://authapi.bboxx.co.uk/v1/authenticate', body: data.to_json, headers:)
      res_body = JSON.parse(res.body)
      @auth_token = res_body['token']
      res_body
    end

    def tokens(mcu_id, token_count)
      authenticate_bbox
      # {
      #     "create_date": "2023-11-20 10:51:16.906283",
      #     "serial_number": "AAM00004869",
      #     "token": "080175819",
      #     "value": 30,
      #     "count": 10,
      #     "mode": 2,
      #     "expected_expiry": "2024-03-08 13:53:44.628452",
      #     "external_id": [
      #         "365a9e1a-d4f3-403c-8556-68a7ac417b6b"
      #     ]
      # }
      res = self.class.get(
        "/v1/credit/token/offline_token/#{'AAM%08d' % mcu_id}?last_token_count=#{token_count}", headers: headers_with_auth
      )

      unless res.code != 200
        body = JSON.parse(res.body)
        is_empty = body['tokens'].blank? or body['tokens'].include?('no tokens found for the device')
        body = is_empty ? [] : body['tokens'].pluck('token')

        return { tokens: body }
      end
      puts "Bbox.tokens Error: " + res.code.to_s + " message: " + res.message
      raise HttpError.new("Failed to fetch tokens from #{self.class.name} for MCU #{mcu_id}", 424, 'FAILED_TO_FETCH_TOKENS_FROM_PROVIDER')
    end
  end
end
