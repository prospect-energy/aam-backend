# frozen_string_literal: true

module PayGoProviders
  class PayGoOps
    include HTTParty
    base_uri 'https://openpaygo-device.paygops.com'

    def initialize(provider, options = {})
      @options = options
      @provider = provider
      @auth = { 'Authorization' => provider.password }
    end

    def headers
      @auth.merge({ 'Content-Type' => 'application/json' })
    end

    def device_data(data)
      res = self.class.post('/api/v2/device_data', body: data.to_json, headers:)
      unless res.code != 200
        tokens = res['tkl'].collect { |token| token.gsub(' ', '') }
        return { tokens: tokens }
      end
      puts "PayGoOps.tokens Error: " + res.code.to_s + " message: " + res.message
      raise HttpError.new("Failed to fetch tokens from #{self.class.name} for MCU #{mcu_id}", 424, 'FAILED_TO_FETCH_TOKENS_FROM_PROVIDER')
    end

    def tokens(mcu_id, token_count)
      device_data({
                    serial_number: 'AAM%08d' % mcu_id,
                    timestamp: Time.now.to_i,
                    data: {
                      token_count:,
                      tampered: false,
                      firmware_version: '1.3.2'
                    },
                    historical_data: {}
                  })
    end
  end
end
