# frozen_string_literal: true

module PayGoProviders
  class Paygee
    include HTTParty
    base_uri 'https://moto.plugintheworld.com'

    def initialize(provider, options = {})
      @options = options
      @provider = provider
      @auth = { 'API-KEY' => provider.password || password[:password] }
    end

    def headers
      @auth.merge({ 'Content-Type' => 'application/json' })
    end

    def tokens(mcu_id, token_count)
      data = { device: "AAM2000/AAM%08d" % mcu_id }
      ## Paygee Malawi changed the prefixes to all the systems expect 7799
      # all future systems will be prefixed with AAM2000/ but the mcu 7799 will still have SG/
      # the 7799 mcu is a test system, and I dont want to create a whole Database solution
      # for a system that won't be used, so Im gonna have this hardcoded patch
      # Im gonna remove this after we are done with Paygee tests and
      # we no longer need 7799
      if mcu_id.to_i == 7799
        data = { device: "SG/AAM%08d" % mcu_id }
      end
      res = self.class.get('/api/v1/tokens/fetch', body: data.to_json, headers:)
      
      unless res.code != 200
        body = JSON.parse(res.body)
        return { tokens: [] } if body.empty? or body.nil? or body.blank?

        filtered_list = body.sort_by { _1['sequence_number'] }
                            .select { _1['sequence_number'] > token_count }
                            .pluck('token')

        return { tokens: filtered_list }
      end

      if res.code == 404
        return { tokens: [] }
      end

      puts "Paygee.tokens Error: " + res.code.to_s + " message: " + res.message
      raise HttpError.new("Failed to fetch tokens from #{self.class.name} for MCU #{mcu_id}", 424, 'FAILED_TO_FETCH_TOKENS_FROM_PROVIDER')
    end
  end
end
