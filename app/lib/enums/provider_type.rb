# frozen_string_literal: true

module Enums
  module ProviderType
    PayGoOps = 'PayGoOps'
    Bbox = 'Bbox'
    Paygee = 'Paygee'
  end
end
