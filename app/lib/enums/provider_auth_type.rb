# frozen_string_literal: true

module Enums
  module ProviderAuthType
    UserPassword = 'UserPassword'
    ApiKey = 'ApiKey'
    AuthToken = 'AuthToken'
  end
end
