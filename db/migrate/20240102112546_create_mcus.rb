# frozen_string_literal: true

# Defines the MCU model structure
# MCU is the microcontroller unit in the AAM that is used to unlock the offset
class CreateMcus < ActiveRecord::Migration[7.1]
  def change
    create_table :mcus do |t|
      t.string :mcu_name, null: false, index: { unique: true }
      t.string :mcu_id, null: false, index: { unique: true }
      t.integer :backend_id, index: true
      t.integer :provider_id, index: true
      t.string :secret_key
      t.timestamps
    end
  end
end
