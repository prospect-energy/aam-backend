# frozen_string_literal: true

# Defines the Providers model structure
# Providers are the different backend providers that can be used to unlock the MCU
class CreateProviders < ActiveRecord::Migration[7.1]
  def change
    create_table :providers do |t|
      t.string :provider_name, null: false, index: { unique: true }
      t.string :unlock_type, null: false
      t.string :url
      t.string :username
      t.string :password
      t.string :auth_type
      t.timestamps
    end
  end
end
