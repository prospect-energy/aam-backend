class AddColumnsToMcu < ActiveRecord::Migration[7.1]
  def change
    add_column :mcus, :starting_token, :string
    add_column :mcus, :status, :integer, default: 0
    add_column :mcus, :token_count, :integer, default: 0
    add_column :mcus, :updated_on,  :date
    add_column :mcus, :mcu_version, :string, default: "v3"

    # add_column :mcus, refe
    add_reference :mcus, :organization

    remove_column :mcus, :backend_id
  end
end
