# frozen_string_literal: true

# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).

# We just load all the data from data directory
#
if !ENV['ADMIN_EMAIL'].blank? && !ENV['ADMIN_PASS'].blank?
  email = ENV['ADMIN_EMAIL']
  pass = ENV['ADMIN_PASS']
  puts "ADMIN_EMAIL and ADMIN_PASS are both present, creating admin user #{email}"
  AdminUser.create!(email: email, password: pass,
                    password_confirmation: pass)
end
